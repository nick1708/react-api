import React, {Component, Fragment} from 'react'

class InfoUser extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
            <Fragment>
                <div className="col-xl-2">
                    <div className="card" style={{width: "18rem"}}>
                        <img className="card-img-top" src={this.props.info.avatar} alt="Card image cap" />
                        <div className="card-body">
                            <h5 className="card-title">{`${this.props.info.first_name} ${this.props.info.last_name}`}</h5>
                            <p className="card-text">{this.props.info.email}</p>
                            <a href={`mailto:${this.props.info.email}`} className="btn btn-primary">Send email</a>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default InfoUser;