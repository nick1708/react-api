import React, {Component, Fragment} from 'react';
import axios from 'axios';
import InfoUser from './infoUser';

class Users extends Component{
    constructor(props){
        super(props);
        this.state={
            users:[]
        }
    }

    componentDidMount(){
        this.fetchData();
    }

    fetchData = async _ =>{
        const {data: {data}} = await axios.get("https://reqres.in/api/users?page=1");
        this.setState(prevState => {
            return {
                users: data
            }
        })
    }

    listUsers = _ =>{
        // console.log(item);
        return this.state.users.map(item => {
            return <InfoUser info={item}/>         
        });
    }

    render(){
        return(
            <Fragment>
                <h1>Titulo</h1>
                <div className="contaimer">
                    <div className="row">
                        {this.listUsers()}
                    </div>
                </div>
            </Fragment>
        )            
    }
}

export default Users;